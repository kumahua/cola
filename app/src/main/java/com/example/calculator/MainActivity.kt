package com.example.calculator

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.calculator.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var v: ActivityMainBinding
    private var isOperator = false
    private var isDot = false
    private var isDigit = false
    private var bracket = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        v = ActivityMainBinding.inflate(layoutInflater)
        setContentView(v.root)

        v.zero.setOnClickListener { onDigitClick("0") }
        v.one.setOnClickListener { onDigitClick("1") }
        v.two.setOnClickListener { onDigitClick("2") }
        v.three.setOnClickListener { onDigitClick("3") }
        v.four.setOnClickListener { onDigitClick("4") }
        v.five.setOnClickListener { onDigitClick("5") }
        v.six.setOnClickListener { onDigitClick("6") }
        v.seven.setOnClickListener { onDigitClick("7") }
        v.eight.setOnClickListener { onDigitClick("8") }
        v.nine.setOnClickListener { onDigitClick("9") }

        v.plus.setOnClickListener { onOperatorClick("+") }
        v.minus.setOnClickListener { onOperatorClick("-") }
        v.division.setOnClickListener { onOperatorClick("/") }
        v.star.setOnClickListener { onOperatorClick("*") }

        v.dot.setOnClickListener { onDotClick() }
        v.ac.setOnClickListener { onClearClick() }
        v.del.setOnClickListener { onDelClick() }
        v.equal.setOnClickListener { onEqualClick() }
        v.right.setOnClickListener { onRightClick() }
        v.left.setOnClickListener { onLeftClick() }
    }

    private fun onDigitClick(digit: String) {

        if (!v.input.text.toString().endsWith(")")) {

            v.input.append(digit)
            isDigit = true
            isOperator = false
        }
    }

    private fun onOperatorClick(operator: String) {

        val inputText = v.input.text.toString()

        if (isDigit && !isOperator || inputText.endsWith(")")) {

            v.input.append(operator)
            isDot = false
            isDigit = false
            isOperator = true
        } else if (operator == "-" && !isOperator) {

            v.input.append(operator)
            isOperator = true
        } else if (isOperator && inputText != "-") {

            onDelClick()
            v.input.append(operator)
        }

        if (v.history.text.isNotEmpty()) { v.history.text.clear() }
    }

    private fun onDelClick() {

        val inputText = v.input.text.toString()

        if (inputText.isNotEmpty()) {

            if (inputText.endsWith(".")) { isDot = false }
            else if (inputText.endsWith(")")) { bracket += 1 }
            else if (inputText.endsWith("(")) { bracket -= 1 }
            else if (inputText.endsWith("+")) { isOperator = false }
            else if (inputText.endsWith("*")) { isOperator = false }
            else if (inputText.endsWith("/")) { isOperator = false }
            else if (inputText.endsWith("-")) { isOperator = false }

            v.input.setText(inputText.substring(0, inputText.length - 1))
        }
    }

    private fun onDotClick() {

        if (!isDot && isDigit) {

            v.input.append(".")
            isDot = true
            isDigit = false
        }
    }

    private fun onClearClick() {

        v.input.text.clear()
        v.history.text.clear()
        isOperator = false
        isDot = false
        isDigit = false
        bracket = 0
    }

    private fun onLeftClick() {

        if (v.input.text.isEmpty() || isOperator) {

            v.input.append("(")
            bracket += 1
        }
    }

    private fun onRightClick() {

        if (isDigit && bracket != 0) {

            v.input.append(")")
            bracket -= 1
        }
    }

    private fun onEqualClick() {

        if (bracket != 0 ) { repeat(bracket) { onRightClick() } }

        var value = calculate(v.input.text.toString())

        if (value.contains(".")) { value = value.trimEnd{it == '0'}.trimEnd{it == '.'} }

        v.history.setText("${ v.input.text } = ")
        v.input.setText(value)

        isDigit = true
        if (value.contains(".")) { isDot = true }
    }

    private fun calculate(input: String): String {

        if (input.isEmpty()) return "0"

        var currentNumber = StringBuffer()
        val stack = Stack<Double>()
        var operation = '+'
        var i = 0

        if (input.startsWith('-')) {

            operation = '-'
            i += 1
        }

        while (i <= input.lastIndex) {

            if (Character.isDigit(input[i]) || input[i] == '.') {

                currentNumber = currentNumber.append(input[i])
            } else if (input[i] == '(') {

                val tail = getTail(input.substring(i))
                currentNumber = currentNumber.append(calculate(input.substring(i+1, i+tail)))
                i += tail
            }

            if (input[i] != '.' && !Character.isDigit(input[i]) && !Character.isWhitespace(input[i]) || i == input.lastIndex) {

                when (operation) { //前一個 operation

                    '-' -> {

                        stack.push(-currentNumber.toString().toDouble())
                    }
                    '+' -> {

                        stack.push(currentNumber.toString().toDouble())
                    }
                    '*' -> {

                        stack.push(stack.pop() * currentNumber.toString().toDouble())
                    }
                    '/' -> {

                        stack.push(stack.pop() / currentNumber.toString().toDouble())
                    }
                }

                operation = input[i]
                currentNumber = StringBuffer()
            }

            i++
        }

        var result = 0.toBigDecimal()

        while (!stack.isEmpty()) {

            result += stack.pop().toBigDecimal()
        }

        return result.toString()
    }

    private fun getTail(value: String): Int {

        var count = 0
        var tail = 0

        for (k in 0..value.lastIndex) {

            when (value[k]) {

                '(' -> count++
                ')' -> count--
                else -> continue
            }

            if (count == 0) {

                tail = k
                break
            }
        }

        return tail
    }
}